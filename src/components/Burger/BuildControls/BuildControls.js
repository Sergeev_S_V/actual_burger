import React from 'react';
import './BuildControls.css';
import BuildControl from "./BuildControl/BuildControl";

const types = ['bacon', 'salad', 'cheese', 'meat'];

const BuildControls = props => {
  return (
    <div className="BuildControls">
      <p>Current price: <strong>{props.price}</strong></p>
      {types.map(type => <BuildControl
        key={type}
        type={type}
        added={() => props.ingredientAdded(type)} // 1
        removed={() => props.ingredientRemoved(type)}
        disabled={props.disabled[type]}
      />)}
      <button disabled={!props.purchasable}
              onClick={props.ordered}
              className='OrderButton'>ORDER NOW</button>
    </div>
  )
};

export default BuildControls;